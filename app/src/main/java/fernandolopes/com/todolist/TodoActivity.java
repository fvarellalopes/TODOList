package fernandolopes.com.todolist;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TodoActivity extends AppCompatActivity {

    private List<Item> currentItems;
    public static final String TODO_PREFS = "TodoPrefs";
    public static final String CHK_LIST = "ChkList";
    private Gson gson;
    private LinearLayout chkList;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        chkList = (LinearLayout) findViewById(R.id.idChkLista);
        registerForContextMenu(chkList);

        setSupportActionBar(toolbar);

        gson = new Gson();
        loadPreferences();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialogEditOrNewItem(null);
            }
        });
    }

    private void addItemListView(Item item) {
        final CheckBox chk = new CheckBox(TodoActivity.this);
        chk.setText(item.getTexto());
        chk.setTag(item);
        chk.setLongClickable(true);
        chk.setChecked(item.isMarcado());
        chk.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                position = chkList.indexOfChild(view);
                return false;
            }
        });
        chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                currentItems.get(currentItems.indexOf(chk.getTag())).setMarcado(b);
                savePreferences();
            }
        });

        chkList.addView(chk);
    }

    private void openDialogDeleteItem(final CheckBox view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(TodoActivity.this);
        builder.setTitle(R.string.delete_item);
        builder.setMessage(R.string.msg_delete_item);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteItem(view);
                Toast.makeText(TodoActivity.this, R.string.msg_removido,
                        Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void loadPreferences() {
        SharedPreferences prefs = getSharedPreferences(TODO_PREFS, Context.MODE_PRIVATE);
        String strLoaded = prefs.getString(CHK_LIST, null);
        if (strLoaded != null && !strLoaded.trim().isEmpty()) {
            currentItems = new ArrayList<Item>(Arrays.asList(gson.fromJson(strLoaded, Item[].class)));
            for (Item it : currentItems) {
                addItemListView(it);
            }
        } else {
            currentItems = new ArrayList<Item>();
        }
    }

    private void deleteItem(CheckBox check) {
        currentItems.remove(check.getTag());
        chkList.removeView(check);
        savePreferences();
    }

    private void savePreferences() {
        SharedPreferences prefs = getSharedPreferences(TODO_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(CHK_LIST, gson.toJson(currentItems));
        editor.commit();
    }

    public void addItemPreferences(Item t) {
        currentItems.add(t);
        savePreferences();
    }

    private void openDialogEditOrNewItem(final Item item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(TodoActivity.this);
        builder.setTitle(R.string.new_item);

        final EditText input = new EditText(TodoActivity.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);

        if(item!=null){
            input.setText(item.getTexto());
        }
        builder.setView(input);

        builder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strNewItem = input.getText().toString();
                if(item!=null){
                    item.setTexto(strNewItem);
                    currentItems.get(currentItems.indexOf(item)).setTexto(strNewItem);
                    ((CheckBox)chkList.getChildAt(position)).setText(strNewItem);

                    savePreferences();

                }else{
                    Item editedItem = new Item(System.currentTimeMillis());

                    editedItem.setTexto(strNewItem);

                    addItemPreferences(editedItem);
                    addItemListView(editedItem);
                }
                Toast.makeText(TodoActivity.this, R.string.msg_adicionado,
                        Toast.LENGTH_SHORT).show();


            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.idChkLista) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.edit:
                openDialogEditOrNewItem((Item)chkList.getChildAt(position).getTag());
                return true;
            case R.id.delete:
                openDialogDeleteItem((CheckBox) chkList.getChildAt(position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
